/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 01:09:52 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/23 01:09:55 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_memcmp(const void *s1, const void *s2, size_t n)
{
	size_t	i;
	int		delta;

	i = 0;
	delta = 0;
	while (i < n && delta == 0)
	{
		delta = ((unsigned char *) s1)[i] - ((unsigned char *) s2)[i];
		i++;
	}
	return (delta);
}
