/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 00:11:34 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/27 14:02:46 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static int	ft_strparts(char const *s, char c)
{
	int		part;

	part = 0;
	while (*s != '\0')
	{
		while (*s == c)
			s++;
		if (*s != '\0')
			part++;
		while (*s != c && *s != '\0')
			s++;
	}
	return (part);
}

char	**ft_strsplit(char const *s, char c)
{
	char	**tab;
	int		part;
	int		i;

	part = 0;
	tab = (char **) malloc((ft_strparts(s, c) + 1) * sizeof(char *));
	while (*s != '\0')
	{
		i = 0;
		while (*s == c)
			s++;
		while (s[i] != c && s[i] != '\0')
			i++;
		if (i > 0)
			tab[part++] = ft_strsub(s, 0, i);
		s += i;
	}
	tab[part++] = NULL;
	return (tab);
}
