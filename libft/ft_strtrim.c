/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 00:20:26 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/01 23:20:43 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strtrim(char const *s)
{
	int		i;
	int		len;
	char	*s2;

	i = 0;
	len = 0;
	while (*s == ' ' || *s == '\t' || *s == '\n')
		s++;
	while (s[len + i] != '\0')
	{
		len += i;
		i = 0;
		while (s[len] != ' ' && s[len] != '\t'
			&& s[len] != '\n' && s[len] != '\0')
			len++;
		while (s[len + i] == ' ' || s[len + i] == '\t' || s[len + i] == '\n')
			i++;
	}
	if ((s2 = ft_strnew(len)) == NULL)
		return (NULL);
	i = -1;
	while (++i < len)
		s2[i] = s[i];
	return (s2);
}
