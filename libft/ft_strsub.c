/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/22 23:11:29 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/27 17:01:10 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strsub(char const *s1, unsigned int start, size_t len)
{
	char	*s2;

	if (s1 == NULL)
		return (NULL);
	if ((s2 = ft_strnew(len)) == NULL)
		return (NULL);
	while (len && s2[len--] != '\0')
		s2[len] = s1[(size_t) start + len];
	return (s2);
}

