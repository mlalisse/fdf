/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_realloc.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 05:51:06 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/07 05:59:31 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_realloc(void *s, size_t len, size_t more)
{
	void *more_space = malloc(more);
	void *str = ft_memjoin(s, len, more_space, more);
	ft_putnbr(0);
	free(s);
	ft_putnbr(-1);
	return (str);
}
