/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memmove.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/23 01:10:12 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/23 01:10:13 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	*ft_memmove(void *s1, const void *s2, size_t n)
{
	void	*s3;

	s3 = malloc(n);
	ft_memcpy(s3, s2, n);
	ft_memcpy(s1, s3, n);
	free(s3);
	return (s1);
}
