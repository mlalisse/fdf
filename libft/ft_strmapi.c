/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmapi.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/20 21:59:32 by mlalisse          #+#    #+#             */
/*   Updated: 2013/11/27 11:14:26 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strmapi(char const *s, char (*f)(unsigned int, char))
{
	char	*t;
	char	*t_saved;
	unsigned int	i;

	if (s == NULL || f == NULL)
		return (NULL);
	i = 0;
	t = ft_strnew(ft_strlen(s));
	t_saved = t;
	while (*s != '\0')
		*t++ = (*f)(i++, (char) *s++);
	return (t_saved);
}
