/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lstmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/27 17:29:56 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/01 23:14:41 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

static void	ft_lstpush(t_list **head, t_list *list)
{
	t_list	*cur;

	cur = *head;
	if (*head == NULL)
		*head = list;
	else
	{
		while (cur->next != NULL)
			cur = cur->next;
		cur->next = list;
	}
}

#include "stdio.h"
t_list	*ft_lstmap(t_list *lst, t_list * (*f)(t_list *elem))
{
	t_list	*out;

	out = NULL;
	if (f == NULL)
		return (NULL);
	while (lst != NULL)
	{
		ft_lstpush(&out, f(lst));
		lst = lst->next;
	}
	return (out);
}
