/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itoa.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/25 00:18:01 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/01 20:19:00 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itoa(int n)
{
	int		i;
	int		neg;
	char	*str;

	i = 0;
	neg = 0;
	if (n == -2147483648)
		return (ft_strdup("-2147483648"));
	str = (char *) malloc(12 * sizeof(char));
	str[11] = '\0';
	if ((neg = (n < 0)) == 1)
		n = - n;
	str[11 - ++i] = n % 10 + '0';
	while ((n = n / 10) > 0)
		str[11 - ++i] = n % 10 + '0';
	if (neg)
		str[11 - ++i] = '-';
	return (str + 11 - i);
}
