/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix4.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 21:58:23 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 11:38:02 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"
#include "libft.h"
#include "stdlib.h"
#include "ft_math.h"

t_matrix		*m4_mul_m4(t_matrix *b, t_matrix *a)
{
	t_matrix	*m;

	m = malloc(sizeof(t_matrix));
	m->a = a->a * b->a + a->b * b->e + a->c * b->i + a->d * b->m;
	m->b = a->a * b->b + a->b * b->f + a->c * b->j + a->d * b->n;
	m->c = a->a * b->c + a->b * b->g + a->c * b->k + a->d * b->o;
	m->d = a->a * b->d + a->b * b->h + a->c * b->l + a->d * b->p;
	m->e = a->e * b->a + a->f * b->e + a->g * b->i + a->h * b->m;
	m->f = a->e * b->b + a->f * b->f + a->g * b->j + a->h * b->n;
	m->g = a->e * b->c + a->f * b->g + a->g * b->k + a->h * b->o;
	m->h = a->e * b->d + a->f * b->h + a->g * b->l + a->h * b->p;
	m->i = a->i * b->a + a->j * b->e + a->k * b->i + a->l * b->m;
	m->j = a->i * b->b + a->j * b->f + a->k * b->j + a->l * b->n;
	m->k = a->i * b->c + a->j * b->g + a->k * b->k + a->l * b->o;
	m->l = a->i * b->d + a->j * b->h + a->k * b->l + a->l * b->p;
	m->m = a->m * b->a + a->n * b->e + a->o * b->i + a->p * b->m;
	m->n = a->m * b->b + a->n * b->f + a->o * b->j + a->p * b->n;
	m->o = a->m * b->c + a->n * b->g + a->o * b->k + a->p * b->o;
	m->p = a->m * b->d + a->n * b->h + a->o * b->l + a->p * b->p;
	return ((t_matrix*) m);
}

void		m4_fixmul_m4(t_matrix *b, t_matrix *a)
{
	t_matrix	*m;

	m = malloc(sizeof(t_matrix));
	m->a = a->a * b->a + a->b * b->e + a->c * b->i + a->d * b->m;
	m->b = a->a * b->b + a->b * b->f + a->c * b->j + a->d * b->n;
	m->c = a->a * b->c + a->b * b->g + a->c * b->k + a->d * b->o;
	m->d = a->a * b->d + a->b * b->h + a->c * b->l + a->d * b->p;
	m->e = a->e * b->a + a->f * b->e + a->g * b->i + a->h * b->m;
	m->f = a->e * b->b + a->f * b->f + a->g * b->j + a->h * b->n;
	m->g = a->e * b->c + a->f * b->g + a->g * b->k + a->h * b->o;
	m->h = a->e * b->d + a->f * b->h + a->g * b->l + a->h * b->p;
	m->i = a->i * b->a + a->j * b->e + a->k * b->i + a->l * b->m;
	m->j = a->i * b->b + a->j * b->f + a->k * b->j + a->l * b->n;
	m->k = a->i * b->c + a->j * b->g + a->k * b->k + a->l * b->o;
	m->l = a->i * b->d + a->j * b->h + a->k * b->l + a->l * b->p;
	m->m = a->m * b->a + a->n * b->e + a->o * b->i + a->p * b->m;
	m->n = a->m * b->b + a->n * b->f + a->o * b->j + a->p * b->n;
	m->o = a->m * b->c + a->n * b->g + a->o * b->k + a->p * b->o;
	m->p = a->m * b->d + a->n * b->h + a->o * b->l + a->p * b->p;
	ft_memcpy(b, m, sizeof(t_matrix));
}

t_point		*m4_mul_v4(t_matrix *m, t_point *v)
{
	t_point	*p = malloc(sizeof(t_point));
	float i;
	float j; 
	float k;
	float w;
	i = m->a * v->i + m->b * v->j + m->c * v->k + m->d * v->w;
	j = m->e * v->i + m->f * v->j + m->g * v->k + m->h * v->w;
	k = m->i * v->i + m->j * v->j + m->k * v->k + m->l * v->w;
	w = m->m * v->i + m->n * v->j + m->o * v->k + m->p * v->w;
	p->i = i;
	p->j = j;
	p->k = k;
	p->w = w;
	return (p);
}

t_matrix		*m4_identity()
{
	t_matrix	*m;

	m = ft_memalloc(sizeof(t_matrix));
	m->a = 1;
	m->f = 1;
	m->k = 1;
	m->p = 1;
	return (m);
}

t_matrix	*m4_rot_i(double angle)
{
	t_matrix	*m;

	m = m4_identity();
	m->f = ft_cos(angle);
	m->g = - ft_sin(angle);
	m->j = ft_sin(angle);
	m->k = ft_cos(angle);
	return (m);
}

t_matrix	*m4_rot_j(double angle)
{
	t_matrix	*m;

	m = m4_identity();
	m->a = ft_cos(angle);
	m->c = ft_sin(angle);
	m->i = - ft_sin(angle);
	m->k = ft_cos(angle);
	return (m);
}

t_matrix	*m4_rot_k(double angle)
{
	t_matrix	*m;

	m = m4_identity();
	m->a = ft_cos(angle);
	m->b = - ft_sin(angle);
	m->e = ft_sin(angle);
	m->f = ft_cos(angle);
	return (m);
}

void		rot_i(t_point *p, double angle)
{
	t_matrix	*m;

	m = m4_rot_i(angle);
	m4_mul_v4(m, p);
	free(m);
}

void		rot_j(t_point *p, double angle)
{
	t_matrix	*m;

	m = m4_rot_j(angle);
	m4_mul_v4(m, p);
	free(m);
}

void		rot_k(t_point *p, double angle)
{
	t_matrix	*m;

	m = m4_rot_k(angle);
	m4_mul_v4(m, p);
	free(m);
}

t_point	*v4_new(float i, float j, float k, float w)
{
	t_point		*point;

	point = ft_memalloc(sizeof(t_point));
	point->i = i;
	point->j = j;
	point->k = k;
	point->w = w;
	return (point);
}

void	v4_scale(t_point *point, double n)
{
	point->i *= n;
	point->j *= n;
	point->k *= n;
}

t_matrix	*m4_scale(double n)
{
	t_matrix	*m = m4_identity();
	m->a *= n;
	m->f *= n;
	m->k *= n;
	return (m);
}

void	v4_scale_all(t_point *point, double i, double j, double k)
{
	point->i *= i;
	point->j *= j;
	point->k *= k;
}

t_matrix	*m4_scale_all(double i, double j, double k)
{
	t_matrix	*m = m4_identity();
	m->a *= i;
	m->f *= j;
	m->k *= k;
	return (m);
}

void		v4_translate(t_point *point, t_point *d)
{
	point->i += d->i;
	point->j += d->j;
	point->k += d->k;
	point->w += d->w;
}

t_matrix	*m4_translate(double i, double j, double k)
{
	t_matrix	*m = m4_identity();
	m->d = i;
	m->h = j;
	m->l = k;
	return (m);
}
