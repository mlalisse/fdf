/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 01:07:58 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 11:46:16 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include <fcntl.h>

#include "get_next_line.h"
#include "libft.h"
#include "list.h"
#include "fdf.h"

#define MAX(a, b) ((a > b) ? a : b);

void	isometric_proj(t_point *points, int size)
{
	int		i;

	for (i = 0; i < size; i++)
	{
		points[i].x = (int) (/*(sqrt(2) / (float) 2)*/ 0.707 * (points[i].i - points[i].j)) + WIDTH / 2;
		points[i].y = (int) (/*sqrt((float) 2 / (float) 3)*/ 0.4714 * (- points[i].k) - 0.4082 /* ((float) 1 / sqrt(6)) */ * (points[i].i + points[i].j) + HEIGHT / 2);
	}
}

t_point		*barycentre(t_point *points, int n_point)
{
	int			i;
	t_point		*bar;

	i = 0;
	bar = ft_memalloc(sizeof(t_point));
	while (i < n_point)
	{
		bar->i += points[i].i;
		bar->j += points[i].j;
		bar->k += points[i++].k;
	}
	bar->i /= -i;
	bar->j /= -i;
	bar->k /= -i;

	return (bar);
}

void	add_line(t_object *object, int i, int j)
{
	object->lines[object->lines_len].i = i;
	object->lines[object->lines_len++].j = j;
}

void	init_object(t_object *object)
{
	ft_bzero(object, sizeof(t_object));

	object->points = ft_memalloc(sizeof(t_point) * 120 * 120);
	object->lines = ft_memalloc(sizeof(t_line) * 120 * 240);

	object->m_rot_i = m4_identity();
	object->m_rot_j = m4_identity();
	object->m_rot_k = m4_identity();
	object->m_scale = m4_identity();
	object->m_translate = m4_identity();
}

void	read_object(int fd, char *buf, t_object *object)
{
	while (get_next_line(fd, &buf) != 0)
	{
		while (*buf != '\0')
		{
			if (*buf == ' ' && buf++)
				continue;
			if (object->height == 0)
				object->width++;
			object->points[object->points_len].i = object->height;
			object->points[object->points_len].j = object->points_len % object->width;
			object->points[object->points_len].k = 0.0;
			while ('0' <= *buf && *buf <= '9')
				object->points[object->points_len].k = 10 * object->points[object->points_len].k + (double) (*buf++ - '0');
			object->depth = MAX(object->depth, object->points[object->points_len].k);
			object->points[object->points_len].w = 1.0;
			if (object->points_len % object->width > 0)
				add_line(object, object->points_len - 1, object->points_len);
			if (object->height > 0)
				add_line(object, object->points_len - object->width, object->points_len);
			object->points_len++;
		}
		object->height++;
	}
}

void	load_object(char *map, t_object *object)
{
	t_point		*bar;
	char		*buf;
	int			fd;
	int			i;

	i = 0;
	buf = NULL;
	init_object(object);
	fd = open(map, O_RDONLY);
	read_object(fd, buf, object);

	bar = barycentre(object->points, object->points_len);
	while (i < object->points_len)
	{
	//	v4_scale_all(object->points + i, object->width, object->height, object->depth);
		v4_translate(object->points + i++, bar);
	}
}

int		on_expose(void *vdata)
{
	t_data *data;

	data = vdata;
	write(1, "expose", 5);
	draw(data, data->object);
	return (1);
}

int		main(int argc, char **argv)
{
	t_data		data;

	data.object = malloc(sizeof(t_object));
	ft_bzero(data.object, sizeof(t_object));

	if (argc != 2)
		load_object("42.fdf", data.object);
	else
		load_object(argv[1], data.object);

	if ((data.core = mlx_init()) == NULL)
	{
		ft_putendl("mlx_init failed");
		exit(0);
	}
	if ((data.win = mlx_new_window(data.core, WIDTH, HEIGHT, "Hello")) == NULL)
	{
		ft_putendl("mlx_window_create failed");
		exit(0);
	}
	int bits_per_pixel, size_line, endian = 0;
	data.image = mlx_new_image(data.core, WIDTH, HEIGHT);
	data.img = mlx_get_data_addr(data.image, &bits_per_pixel, &size_line, &endian);
	draw(&data, data.object);
	mlx_hook(data.win, 2, 3, &on_key, (void*) &data);
	mlx_expose_hook(data.win, &on_expose, (void*) &data);
	mlx_loop(data.core);
	return (0);
}
