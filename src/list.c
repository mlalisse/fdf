/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/11/28 22:55:27 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/05 03:53:45 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "list.h"

t_list	*list_new(void *data)
{
	t_list	*new;

	new = (t_list*) malloc(sizeof(t_list));
	new->data = data;
	new->next = NULL;
	return (new);
}

void	list_push(t_list **list, t_list *new)
{
	t_list	*current;

	current = *list;
	if (current == NULL)
		*list = new;
	else
	{
		while (current->next != NULL)
			current = current->next;
		current->next = new;
	}
}

void	*list_pop(t_list **list)
{
	void	*data;
	t_list	*next;

	data = (*list)->data;
	next = (*list)->next;
	free(*list);
	*list = next;
	return (data);
}

void	list_sort_push(t_list **list, t_list *new, int (*cmp)())
{
	t_list	*current;
	
	current = *list;
	if (current && cmp(current, new))
	{
		while (current->next && cmp(current->next, new) < 0)
			current = current->next; 
		new->next = current->next;
		current->next = new;
	}
	else
	{
		new->next = *list;
		*list = new;
	}
}
