/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/09 13:41:44 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 11:55:39 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <mlx.h>
#include "fdf.h"
#include "libft.h"
#include "list.h"

#define D(p1, p2) (p1.x - p2.x) + (p1.y - p2.y)

#define MIN(a, b) ((a > b) ? b : a)

#define MAX(a, b) ((a > b) ? a : b)

	
void	pixel_put(t_data *data, int x, int y, int c)
{
	*((int *)(data->img + (y * WIDTH + x) * 4)) = c;
}

void	draw_line(t_data *data, t_point *p1, t_point *p2)
{
	double	dx = ABS(p2->x - p1->x);
	double	dy = ABS(p2->y - p1->y);
	double	dk = ABS(p2->k - p1->k);
	double	err = 0;
	double	errk = 0;

	t_point		*p3 = malloc(sizeof(t_point));
	ft_memcpy(p3, p1, sizeof(t_point));

	if (((dx > dy && (p3->x > p2->x)) || (dx <= dy && (p3->y > p2->y))))
		draw_line(data, p2, p1);
	if (dx > dy)
	{
		double	derr = dy / dx;
		double	derrk = dk / dx;
		while (p3->x <= p2->x)
		{
			if (0 <= p3->x && p3->x <= WIDTH
				&& 0 <= p3->y && p3->y <= HEIGHT)
			{
				int c = ((int) ((512 * data->object->depth) / ABS(p3->k)));
				//c |= 0x7;
				c |= ((int) ((512 * data->object->height) / ABS(p3->j))) << 8;
				c |= ((int) ((512 * data->object->width) / ABS(p3->i))) << 16;
				pixel_put(data, p3->x, p3->y, c);
			}
			err += derr;
			errk += derrk;
			if (err > 0.5 && err--)
				p3->y += (p1->y < p2->y) ? 1 : -1;
			if (errk > 0.5 && errk--)
				p3->k += (p1->k < p2->k) ? 1 : -1;
			p3->x++;
		}
	}
	else
	{
		while (p3->y <= p2->y)
		{
			double	derr = dx / dy;
			double	derrk = dk / dx;
			if (0 <= p3->x && p3->x <= WIDTH
				&& 0 <= p3->y && p3->y <= HEIGHT)
			{
				int c = ((int) ((512 * data->object->depth) / ABS(p3->k)));
				//c |= 0x7;
				c |= ((int) ((512 * data->object->height) / ABS(p3->j))) << 8;
				c |= ((int) ((512 * data->object->width) / ABS(p3->i))) << 16;
				pixel_put(data, p3->x, p3->y, c);
			}
			err += derr;
			errk += derrk;
			if (err > 0.5 && err--)
				p3->x += (p1->x < p2->x) ? 1 : -1;
			if (errk > 0.5 && errk--)
				p3->k += (p1->k < p2->k) ? 1 : -1;
			p3->y++;
		}
	}
	free(p3);
}

void	draw_object(t_data *data, t_object *object)
{
	int			i = 0;
	t_point		p[120 * 120];

	t_matrix	*tmp;
	t_matrix	*m;

	tmp = m4_mul_m4(object->m_scale, object->m_rot_i);
	tmp = m4_mul_m4(tmp, object->m_rot_j);
	tmp = m4_mul_m4(tmp, object->m_rot_k);
	m = m4_mul_m4(tmp, object->m_translate);
	free(tmp); // 4
	while (i < object->points_len)
	{
		t_point *res = m4_mul_v4(m, object->points + i);

		res->x = res->i + WIDTH / 2;
		res->y = res->j + HEIGHT / 2;

		ft_memcpy(p + i, res, sizeof(t_point));
		i++;
	}
	//isometric_proj(p, object->points_len);

	i = 0;
	while (i < object->lines_len)
	{
		draw_line(data, p + object->lines[i].i, p + object->lines[i].j);
		i++;
	}
}

void	draw(t_data *data, t_object *object)
{
	ft_bzero(data->img, WIDTH * HEIGHT * 4);
	draw_object(data, object);
	mlx_clear_window(data->core, data->win);
	mlx_put_image_to_window(data->core, data->win, data->image, 0, 0);
}
