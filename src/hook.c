/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hook.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/07 10:19:08 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 11:44:04 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <mlx.h>
#include "libft.h"
#include "fdf.h"

int		on_key(int keycode, void *vdata)
{
	t_point	*points;
	int		n_point;
	t_data	*data = vdata;

	points = data->object->points;
	n_point = data->object->points_len;

	if (keycode == 'q' || keycode == 65307)
		exit(0);

	if (keycode == 65361) //left
		m4_fixmul_m4(data->object->m_translate, m4_translate(0, 10, 0));

	if (keycode == 65362) // up
		m4_fixmul_m4(data->object->m_translate, m4_translate(10, 0, 0));

	if (keycode == 65363) // right
		m4_fixmul_m4(data->object->m_translate, m4_translate(0, -10, 0));

	if (keycode == 65364) // down
		m4_fixmul_m4(data->object->m_translate, m4_translate(-10, 0, 0));

	if (keycode == 61) /* + */
		m4_fixmul_m4(data->object->m_scale, m4_scale((double) 10 / 9));

	if (keycode == 45) /* - */
		m4_fixmul_m4(data->object->m_scale, m4_scale((double) 9 / 10));

	if (keycode == 48) /* 9 */
		m4_fixmul_m4(data->object->m_scale, m4_scale_all(1, 1, (double) 6 / 5));
	if (keycode == 57) /* 0 */
		m4_fixmul_m4(data->object->m_scale, m4_scale_all(1, 1, (double) 5 / 6));
	if (keycode == 104) /* h */
		m4_fixmul_m4(data->object->m_rot_i, m4_rot_i(3.14151836715450 / (double) 6));
	if (keycode == 106)/* j */
		m4_fixmul_m4(data->object->m_rot_j, m4_rot_j(3.14151836715450 / (double) 6));
	if (keycode == 107)/* k */
		m4_fixmul_m4(data->object->m_rot_j, m4_rot_j(- 3.14151836715450 / (double) 6));
	if (keycode == 108)/* l */
		m4_fixmul_m4(data->object->m_rot_i, m4_rot_i(- 3.14151836715450 / (double) 6));

	if (keycode == 109)/* n */
		m4_fixmul_m4(data->object->m_rot_k, m4_rot_k(- 3.14151836715450 / (double) 6));

	if (keycode == 110)/* m */
		m4_fixmul_m4(data->object->m_rot_k, m4_rot_k(3.14151836715450 / (double) 6));
	
	ft_putnbr(keycode);
	ft_putstr("\n");
	mlx_clear_window(data->core, data->win);
	draw(data, data->object);

	return (1);
}

