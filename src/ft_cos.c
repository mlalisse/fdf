/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cos.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 04:42:31 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 06:05:40 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_math.h"

float		ft_cos(double x)
{
	int     i;
	int		denominator;
	float	nominator;
	float	res;

	i = 0;
	res = 0;
	nominator  = 1;
	denominator = 1;

	while (i++ < TAYLOR_LIMIT)
	{
		res += nominator / denominator;
		nominator   *= -1 * x * x ;
		denominator *= (2 * i) * (2 * i - 1); // i * i + 2 * i + 1
	}
	return (res);
}
