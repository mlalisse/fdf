/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sin.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 05:05:02 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/11 06:32:43 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_math.h"

float		ft_sin(double x)
{
	int     i;
	int		denominator;
	float	nominator;
	float	res;

	i = 0;
	res = 0;
	nominator  = x;
	denominator = 1;

	while (i++ < TAYLOR_LIMIT)
	{
		res += ((float) nominator) / denominator;
		nominator   *= -1 * x * x ;
		denominator *= (2 * i) * (2 * i + 1); // i * i + 2 * i + 1
	}
	return (res);
}
