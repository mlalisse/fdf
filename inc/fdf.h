/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   matrix4.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 21:58:20 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/22 11:41:02 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX4_H
# define MATRIX4_H

# define ABS(x) ((x >= 0) ? x : -(x))

#define WIDTH 2500
#define HEIGHT 2000

typedef struct
{
	float	a;
	float	b;
	float	c;
	float	d;

	float	e;
	float	f;
	float	g;
	float	h;

	float	i;
	float	j;
	float	k;
	float	l;

	float	m;
	float	n;
	float	o;
	float	p;
}					t_matrix;

typedef struct	s_point
{
	double	i;
	double	j;
	double	k;
	double	w;
	int		x;
	int		y;
	int		color;
	//struct s_list *voisins;
}				t_point;

typedef struct	s_line
{
	int		i;
	int		j;
}				t_line;

typedef struct	s_object
{
	t_point 	*points;
	int			points_len;

	t_line		*lines;
	int			lines_len;

	int			width;
	int			height;
	int			depth;

	int			width_min;
	int			height_min;
	int			depth_min;

	t_matrix	*m_rot_i;
	t_matrix	*m_rot_j;
	t_matrix	*m_rot_k;
	t_matrix	*m_scale;
	t_matrix	*m_translate;
}				t_object;

typedef struct	s_data
{
	void		*core;
	void		*win;
	t_point 	points[120 * 120];
	t_object	*object;
	void		*image;
	char		*img;
	int			n;
}				t_data;

float		ft_cos(double x);

float		ft_sin(double x);

void		v4_scale(t_point *point, double n);

void		v4_scale_all(t_point *point, double i, double j, double k);

void		v4_translate(t_point *point, t_point *d);

t_matrix	*m4_mul_m4(t_matrix *a, t_matrix *b);

t_point		*m4_mul_v4(t_matrix *m, t_point *v);

void		m4_fixmul_m4(t_matrix *b, t_matrix *a);

t_matrix	*m4_identity();

t_matrix	*m4_scale(double n);

t_matrix	*m4_scale_all(double i, double j, double k);

t_matrix	*m4_rot_i(double angle);

void		rot_i(t_point *p, double angle);

t_matrix	*m4_rot_j(double angle);

void		rot_j(t_point *p, double angle);

t_matrix	*m4_rot_k(double angle);

void		rot_k(t_point *p, double angle);

t_matrix	*m4_translate(double i, double j, double k);

int			on_key(int keycode, void *data);

t_point		*barycentre(t_point *points, int n_point);

void		print_points(t_point *points, int n_point);

void		draw_line(t_data *data, t_point *p1, t_point *p2);

void		draw_object(t_data *data, t_object *object);

void		draw(t_data *data, t_object *object);

t_matrix	*camera(t_point *pos, t_point *way, t_point *up);

void		isometric_proj(t_point *points, int size);

#endif
