/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/06 21:58:20 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/07 07:30:45 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MATRIX4_H
#define MATRIX4_H

typedef struct
{
	float	a;
	float	b;
	float	c;
	float	d;
	float	e;
	float	f;
	float	g;
	float	h;
	float	i;
	float	j;
	float	k;
	float	l;
	float	m;
	float	n;
	float	o;
	float	p;
}					t_matrix;

typedef struct	s_point
{
	double	i;
	double	j;
	double	k;
	double	w;
	int		x;
	int		y;
	t_list	*voisins;
}				t_point;

#endif
