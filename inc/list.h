/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/05 02:49:07 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/05 11:23:46 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

typedef struct		s_list
{
	void			*data;
	struct s_list	*next;
}					t_list;

t_list	*list_new(void *data);

void	list_push(t_list **list, t_list *new);
