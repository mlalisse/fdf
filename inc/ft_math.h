/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_math.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2013/12/11 04:42:34 by mlalisse          #+#    #+#             */
/*   Updated: 2013/12/11 06:40:14 by mlalisse         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_MATH_H
# define FT_MATH_H

# define TAYLOR_LIMIT 6
# define SQRT_LIMIT 10

float			ft_sin(double x);

float			ft_cos(double x);

inline float	ft_tan(double x);

float			ft_sqrt(double x);

#endif /* !FT_MATH_H */
