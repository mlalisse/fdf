# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: mlalisse <mlalisse@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/12/22 05:26:04 by mlalisse          #+#    #+#              #
#    Updated: 2013/12/22 07:21:58 by mlalisse         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

CC = gcc
CFLAGS = -Wall -Wextra -Werror

LIB = main.c list.c matrix4.c hook.c draw.c get_next_line.c ft_cos.c ft_sin.c
SRC = $(addprefix src/, $(LIB))

MLX = -I/usr/X11/include -L/usr/X11/lib -lXext -lX11 -lmlx
LFT = -lft -L libft -I libft/include

NAME = fdf

all: $(NAME)

$(NAME): $(SRC)
	$(CC) -o $(NAME) $(SRC) $(CFLAGS) $(MLX) $(LFT) -I inc 

libft:
	make -C libft

clean:
	make -C libft clean

fclean: clean
	rm -f $(NAME)

re: clean all
